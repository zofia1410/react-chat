import {connect} from "react-redux";
import MessageList from "../components/MessageList"

const getMessages = (state) => state.messages.messages;

const mapStateToProps = (state) => ({
    messages: getMessages(state),
});

export default connect(mapStateToProps)(MessageList);