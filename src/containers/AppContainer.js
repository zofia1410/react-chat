import {connect} from "react-redux";
import * as actions from "../actions";
import App from "../components/App";

export default connect(
    null,
    actions
)(App);
