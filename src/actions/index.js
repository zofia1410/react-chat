import { ADD_MESSAGE, GET_MESSAGES_REQUEST } from "./types";

export const addMessage = message => {
  return {
    type: ADD_MESSAGE,
    message: message
  };
};

export const getMessages = () => {
  return {
    type: GET_MESSAGES_REQUEST
  };
};
