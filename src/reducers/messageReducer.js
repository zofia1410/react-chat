import {ADD_MESSAGE, GET_MESSAGES_SUCCESS} from "../actions/types";

const initialState = {
  messages: [
    // {
    //   author: faker.name.firstName(1),
    //   time: new Date().toLocaleTimeString(),
    //   message: "Hello!",
    //   avatar: faker.image.avatar()
    // }
  ]
};

export default function(state = initialState, action) {
  console.log(action);
  switch (action.type) {
    case ADD_MESSAGE:
      return {
        ...state,
        messages: [...state.messages, action.message]
      };
    case GET_MESSAGES_SUCCESS:
      return {
        ...state,
        messages: action.messages
      };
    default:
      return state;
  }
}
