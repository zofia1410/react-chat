import {call, put, takeLatest} from "redux-saga/effects"
import * as actions from "../actions/types"
import {getPosts, getUsers} from "../api/jsonPlaceholder"
import faker from "faker";

function* fetchMessages() {
    try {
        const response = yield call(getPosts);
        const users = yield call(getUsers);
        const messages = response.data.map(message => {
            const user = users.data.find(user => {
                return user.id === message.userId;
            });
            return {
                author: user.name,
                message: message.body,
                avatar: faker.image.avatar()
            }
        });
        yield put({type: actions.GET_MESSAGES_SUCCESS, messages: messages})
    } catch (err) {
        yield put({type: actions.GET_MESSAGES_FAILURE, error: err})
    }
}

function* messageSaga() {
    yield takeLatest(actions.GET_MESSAGES_REQUEST, fetchMessages);
}

export default messageSaga;