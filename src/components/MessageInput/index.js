import React from "react";

class Index extends React.Component {
  state = { messageText: "" };

  onMessageSubmit(event) {
    event.preventDefault();
    this.props.onSubmit(this.state.messageText);
    this.setState({ messageText: "" });
  }

  render() {
    return (
      <div className="ui segment">
        <form onSubmit={e => this.onMessageSubmit(e)} className="ui form">
          <div className="field">
            <label>Enter Message</label>
            <input
              type="text"
              value={this.state.messageText}
              onChange={e => this.setState({ messageText: e.target.value })}
              required="required"
            />
          </div>
        </form>
      </div>
    );
  }
}

export default Index;
