import React from "react";
import MessageItem from "../MessageItem";
import "./styles.css";

class MessageList extends React.Component {
  messagesEnd = React.createRef();

  displayMessages = messages =>
    messages.map((message, i) => (
      <MessageItem
        key={i}
        author={message.author}
        time={message.time}
        message={message.message}
        avatar={message.avatar}
      />
    ));

  scrollToBottom = () => {
    this.messagesEnd.current.scrollIntoView({ behavior: "auto" });
  };

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  render() {
    return (
      <div className="message-list-container">
        <div className="ui container comments message-list">
          {this.displayMessages(this.props.messages)}
          <div ref={this.messagesEnd} />
        </div>
      </div>
    );
  }
}

export default MessageList;
