import React from "react";

const Index = props => {
  return (
    <div className="comment" style={{ bottom: "0" }}>
      <a href="/" className="avatar">
        <img alt="avatar" src={props.avatar} />
      </a>
      <div className="content">
        <a href="/" className="author">
          {props.author}
        </a>
        <div className="text">{props.message}</div>
      </div>
    </div>
  );
};

export default Index;
