import React from "react";
import faker from "faker";
import MessageInput from "../MessageInput";
import MessageListContainer from "../../containers/MessageListContainer";
import "./styles.css";

class Index extends React.Component {

  onMessageSubmit(messageText) {
    this.props.addMessage({
      author: faker.name.firstName(2),
      message: messageText,
      avatar: faker.image.avatar()
    });
  }

   componentDidMount() {
    this.props.getMessages();
  }

  render() {
    return (
      <React.Fragment>
        <MessageListContainer />
        <div className="ui container message-input">
          <MessageInput onSubmit={message => this.onMessageSubmit(message)} />
        </div>
      </React.Fragment>
    );
  }
}

export default Index;
