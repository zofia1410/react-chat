import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com',
});

export function getPosts () {
    return instance.get('/users/1/posts')
}

export function getUsers() {
    return instance.get(`/users`)

}